#!/bin/bash

# Author: Faro BLAU <faro.blau@gmail.com>
# A thankful mail or a donation through Paypal would be greatly appreciated
# https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6HMW5UZZTABQU
# 
# This script allows to run twitch with a popup displaying which stream to be followed
# 
# httplive debug: candidate 0 bandwidth (bits/s) 2477665 >= 242144
# httplive debug: candidate 1 bandwidth (bits/s) 2477665 >= 651232
# httplive debug: candidate 2 bandwidth (bits/s) 2477665 >= 1266368
# httplive debug: candidate 3 bandwidth (bits/s) 2477665 >= 1881504

#https://github.com/justintv/Twitch-API/blob/master/authentication.md
#https://api.twitch.tv/kraken/oauth2/authorize?response_type=token&client_id=t54rd2vs8vq0ir3q1oxffbew9ib45yf&redirect_uri=http://localhost&scope=user_read+user_subscriptions
#access_token=XXXXXXXXX&scope=user_read+user_subscriptions
#use XXXXX below


oauthFile="$HOME/.config/twitch-scripts/twitch-oauth"
read oauth < $oauthFile
if [ -z $oauth ]; then
  echo "Run on firefox"
  echo "This retrieve an URL with a format access_token=XXXXXXXXX&scope=user_read+user_subscriptions"
  echo "Paste then the XXXXX here:"
  echo "https://api.twitch.tv/kraken/oauth2/authorize?response_type=token&client_id=t54rd2vs8vq0ir3q1oxffbew9ib45yf&redirect_uri=http://localhost&scope=user_read+user_subscriptions"
  
  read oauth
  mkdir -p "$(dirname $oauthFile)"
  echo "$oauth" > $oauthFile
fi

#array=`while read first; do read second; echo "$second"; done < ~/.local/share/glipper/plugins/snippets | tr "\n" " "`
array=$(curl -H 'Accept: application/vnd.twitchtv.v3+json' -H "Authorization: OAuth $oauth" -X GET https://api.twitch.tv/kraken/streams/followed 2>/dev/null | grep -Po '"name":.*?[^\\]"' | cut -d "\"" -f4 | tr "\n" " ")

my=$(zenity --entry --title "Twitch" --text "Which live stream:" $array ' ')

if [ ! -z "$my" ]; then
    quality=$(livestreamer twitch.tv/$my  | grep "Available streams" | grep -Po '[^,]+best' | cut -d "(" -f1 | tr -d '[[:space:]]')
    if [ -z "$quality" ]; then
      quality="high"
    fi
    livestreamer --http-header Client-ID=ewvlchtxgqq88ru9gmfp1gmyt6h2b93 twitch.tv/$my $quality
fi

